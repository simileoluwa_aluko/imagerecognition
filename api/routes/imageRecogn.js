const express = require('express')
const router = express.Router()
const fileHandler = require('../middleware/fileHandler')
const imageRecognHandler = require('../middleware/img-recogn-handler')

router.post('/', fileHandler, imageRecognHandler, (req, res, next)=> {

})

module.exports = router