const fs = require('fs')
const automl = require('@google-cloud/automl')
const imageToBase64 = require('image-to-base64')

const predictionClient = new automl.PredictionServiceClient({
    keyFilename: 'automlkeyfile.json'
})

const modelFullId = predictionClient.modelPath(
// google cloud project,
//location, 
//automl model id
)
const scoreThreshold = '0.5'
const params = {}
if (scoreThreshold) {
    params.score_threshold = scoreThreshold
}

module.exports = (req, res, next) => {
    try {
        const image = req.files['image'][0].path

        imageToBase64(image)
            .then((result) => {

                const requestBody = {
                    name: modelFullId,
                    payload: {
                        'image': {
                            'imageBytes': result
                        }
                    },
                    params: params
                }

                predictionClient.predict(requestBody)
                    .then(response => {
                        res.status(200).json({
                            message: 'prediction successful',
                            predictionResult: response
                        })
                        fs.unlink(image, error => {
                            if (error) throw error;
                            console.log(`${image} deleted`);
                        })
                    }).catch(error => {
                        res.status(500).json({
                            error: 'error making predictions : ' + err
                        })
                        fs.unlink(image, error => {
                            if (error) throw error;
                            console.log(`${image} deleted`);
                        })
                    })
            }).catch((err) => {
                res.status(500).json({
                    error: 'error reading waec file for image recognition : ' + err
                })
                fs.unlink(image, error => {
                    if (error) throw error;
                    console.log(`${image} deleted`);
                })
            });
    } catch (error) {
        res.status(500).json({
            error: 'error processing request : ' + error
        })
    }

}