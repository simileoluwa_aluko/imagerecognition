'use strict'

const http = require('http')
const app = require('./app')
const portNumber = require('./utils').portNumber
const port = process.env.port || portNumber
const server = http.createServer(app)
server.listen(port)