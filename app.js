const express = require('express')
const app = express()
const cors = require('cors')
const morgan = require('morgan')

const imageRecognitionRoute = require('./api/routes/imageRecogn')

app.use('/uploads', express.static('uploads'))
app.use(cors())
app.use(morgan('dev'))

app.use('/image-recognition', imageRecognitionRoute)

app.use((req, res, next) => {
    const error = new Error('request not found')
    error.status = 404
    next(error)
})

app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error : {
            message : error.message 
        }
    })
})
module.exports = app